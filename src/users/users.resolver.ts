import {ResolveField, Resolver} from '@nestjs/graphql';
import {PostsService} from "../posts/posts.service";

@Resolver('User')
export class UsersResolver {
    constructor(private postsService: PostsService) {}

    @ResolveField('posts')
    getPostsByUsers(): any {
        return this.postsService.findAll()
    }
}

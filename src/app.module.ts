import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {GraphQLFederationModule} from "@nestjs/graphql";
import { join } from 'path';
import {PostsService} from "./posts/posts.service";
import {PostsResolver} from "./posts/posts.resolver";
import { UsersResolver } from './users/users.resolver';

@Module({
  imports: [
    GraphQLFederationModule.forRoot({
      debug: true,
      playground: true,
      typePaths: ['**/*.graphql'],
      definitions: {
        path: join(process.cwd(), 'src/models/graphql.ts'),
      },
    })
  ],
  controllers: [AppController],
  providers: [AppService, PostsService, PostsResolver, UsersResolver],
})
export class AppModule {}

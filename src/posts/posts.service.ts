import { Injectable } from '@nestjs/common';

@Injectable()
export class PostsService {
    findAll(): any {
        console.log('find all post')
        return [
            {
                id: 'post-01',
                title: 'Post title',
                body: 'String!',
                user: '00001'
            }
        ]
    }

    findById(id: string): any {
        console.log('find id post')
        return {
            id: 'post-02',
            title: 'Post title',
            body: 'String!',
            user: '00001'
        }
    }
}

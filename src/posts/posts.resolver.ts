import {Parent, Query, ResolveField, Resolver} from '@nestjs/graphql';
import {PostsService} from "./posts.service";
import {Post} from "../models/graphql";

@Resolver("Post")
export class PostsResolver {
    constructor(private postsService: PostsService) {}

    @Query('getPosts')
    getPosts() {
        return this.postsService.findAll();
    }

    @ResolveField('user')
    getUser(@Parent() post: Post) {
        return { __typename: 'User', id: post.user };
    }
}

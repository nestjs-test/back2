
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface Post {
    id: string;
    title: string;
    body: string;
    user?: Nullable<User>;
}

export interface User {
    id: string;
    posts?: Nullable<Nullable<Post>[]>;
}

export interface IQuery {
    getPosts(): Nullable<Nullable<Post>[]> | Promise<Nullable<Nullable<Post>[]>>;
}

type Nullable<T> = T | null;
